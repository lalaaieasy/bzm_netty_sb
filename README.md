# bzm_netty_sb

### ### 不忘初心

  **此项目netty4.x主要是为了让更多初学者、学习、了解netty、使用、关于集群到时候会再开一个项目 、敬请期待 求star 和 关注**                             
  **【 想学习、或提升、java网络编程，bio、nio、aio、netty 的可以联系我QQ: 68001531  我可以指导、分享学习方法 】**  

### #### 介绍                                                                               

`已完成简单功能： 1、登陆 ;2、群消息发送;3、好友单独发消息、可同时多个好友发送`                                    
`后期会继续更新；大家的star就是作者的动力 ， 支持百万并发`                        


### #### 软件架构

  **webSocket、netty4、springboot、mybatis-puls、mysql8.0、layui、jquery  **      
  **使用mysql存储，只是为了方便初学者,更方便、快捷的把项目跑起来 ** 

### ### 志同道可

  ** 有前端大佬, 可以一起合作 一起挑战百万并发 、只要给加班费,当牛做马无所谓、 求聊天系统ui模板**           
  

### #### 安装教程

1.   **jdk1.8  、 mysql5.7x++ 、netty、springboot        **     
2.   **1. 下载项目 既可运行 ； 开多个网页浏览器 登陆不同账号即可实现聊天功能   ** 
3.   **项目默认访问地址是  http://127.0.0.1:8081  ；  netty服务启动的是 8080端口**  



### #### 联系方式
                                                                                                                                                                             
 **1、演示地址 [http://netty.chechaibao.com/](http://netty.chechaibao.com/)   ** 或者 http://47.105.74.21:8081
                                                                                                                             
 
**2、 作者 邮箱 hm68001531@163.com , QQ交流群 695449401 , 1071066691  **     


### ### 操作流程图 
              
                                                                                                                          
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/205328_ff57c433_1317781.png "im_聊天0000.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/205346_2afb2521_1317781.png "im聊天001.png")


![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/205358_e2579049_1317781.png "im聊天002.png")



### ### 感谢大家的支持 和  star


![输入图片说明](https://images.gitee.com/uploads/images/2020/0609/160648_c9c5cecd_1317781.png "啦啦啦啦啦8.png")


### ### 我的微信 加我的 拉你进微信群


![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/100208_325d32de_1317781.png "我的微信wx8.png")

                                                

