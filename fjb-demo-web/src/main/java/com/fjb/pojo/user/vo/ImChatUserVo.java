package com.fjb.pojo.user.vo;

import com.fjb.pojo.user.SysUser;

/**
 * @Description:TODO
 * @author hemiao
 * @time:2020年5月27日 上午10:41:00
 */
public class ImChatUserVo {
	
	private SysUser user;
	
	/**
	 * 登录时获得ip地址
	 */
	private String loginIpLocation;

	public SysUser getUser() {
		return user;
	}

	public void setUser(SysUser user) {
		this.user = user;
	}

	public String getLoginIpLocation() {
		return loginIpLocation;
	}

	public void setLoginIpLocation(String loginIpLocation) {
		this.loginIpLocation = loginIpLocation;
	}
}
