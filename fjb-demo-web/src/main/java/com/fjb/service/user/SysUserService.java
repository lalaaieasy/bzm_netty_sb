package com.fjb.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjb.pojo.user.SysUser;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author hemiao
 * @since 2019-12-22
 */
public interface SysUserService extends IService<SysUser> {
	

}
