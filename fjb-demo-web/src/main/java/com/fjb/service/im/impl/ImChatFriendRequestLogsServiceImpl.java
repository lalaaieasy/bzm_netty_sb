package com.fjb.service.im.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.mapper.im.ImChatFriendInfoMapper;
import com.fjb.mapper.im.ImChatFriendRequestLogsMapper;
import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.ImChatFriendRequestLogs;
import com.fjb.pojo.im.vo.ImChatFriendRequestLogsVo;
import com.fjb.service.im.ImChatFriendRequestLogsService;

/**
 * <p>
 * 好友请求记录 服务实现类
 * </p>
 *	
 * @author hemiao
 * @since 2020-06-02
 */
@Service
public class ImChatFriendRequestLogsServiceImpl extends ServiceImpl<ImChatFriendRequestLogsMapper, ImChatFriendRequestLogs> implements ImChatFriendRequestLogsService {

	@Autowired
	private ImChatFriendRequestLogsMapper imChatFriendRequestLogsMapper;
	@Autowired
	private ImChatFriendInfoMapper imChatFriendInfoMapper;
	
	@Override
	public List<ImChatFriendRequestLogsVo> selectRequestLogsList(Integer userId) {
		return imChatFriendRequestLogsMapper.selectRequestLogsList(userId);
	}
	
	@Transactional
	@Override
	public JsonResult<Boolean> updateRequestStatus1(ImChatFriendRequestLogs updateLogs, ImChatFriendInfo fi) {
		Integer friendUserId = fi.getFriendUserId();
		Integer userId = fi.getUserId();
		
		imChatFriendRequestLogsMapper.updateById(updateLogs);
		imChatFriendInfoMapper.insert(fi);	
			
		fi.setUserId(friendUserId);
		fi.setFriendUserId(userId);
		fi.setMainUserId(null);
		imChatFriendInfoMapper.insert(fi);
		return new JsonResult<Boolean>(null, HttpCode.SUCCESS);
	}
	
	
}
