package com.fjb.service.im;

import com.fjb.pojo.im.ImChatFriendGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 朋友组 服务类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendGroupService extends IService<ImChatFriendGroup> {

}
