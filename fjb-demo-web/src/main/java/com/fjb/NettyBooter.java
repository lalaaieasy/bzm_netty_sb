package com.fjb;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.fjb.config.im.NettyServer;


@Component
public class NettyBooter implements ApplicationListener<ContextRefreshedEvent>{

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			try {
				NettyServer.getInstance().start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
