package com.fjb.mapper.im;

import com.fjb.pojo.im.ImChatFriendGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 朋友组 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendGroupMapper extends BaseMapper<ImChatFriendGroup> {

}
