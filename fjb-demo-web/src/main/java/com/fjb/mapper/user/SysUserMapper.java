package com.fjb.mapper.user;

import com.fjb.pojo.user.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2019-12-22
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
